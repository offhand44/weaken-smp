package life.weaken.smp;

import life.weaken.core.CorePlugin;
import life.weaken.smp.drop.DropListener;
import life.weaken.smp.drop.DropManager;
import life.weaken.smp.misc.*;
import life.weaken.smp.randomtp.RandomTpCommand;
import life.weaken.smp.randomtp.RandomTpListener;
import life.weaken.smp.randomtp.RandomTpManager;
import life.weaken.smp.reclaim.ReclaimCommand;
import life.weaken.smp.reclaim.ReclaimManager;
import life.weaken.smp.reclaim.ResetReclaimCommand;
import life.weaken.smp.shop.ShopCommand;
import life.weaken.smp.shop.ShopManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class SMPPlugin extends JavaPlugin {

    private CorePlugin corePlugin;

    private DropManager dropManager;
    private RandomTpManager randomTpManager;
    private ReclaimManager reclaimManager;
    private ShopManager shopManager;

    @Override
    @SuppressWarnings("ConstantConditions")
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new GenerationListener(this), this);

        Economy economy = getServer().getServicesManager().getRegistration(Economy.class).getProvider();

        corePlugin = (CorePlugin) grabDependency("Weaken-Core");
        dropManager = new DropManager(this);
        randomTpManager = new RandomTpManager(this, corePlugin.getServerDataStorage());
        reclaimManager = new ReclaimManager(this);
        shopManager = new ShopManager(this, economy);

        saveDefaultConfig();

        registerCommands();
        registerListeners(getConfig());
    }

    @Override
    public void onDisable() {
        randomTpManager.shutdown();
    }

    @SuppressWarnings("ConstantConditions")
    private void registerCommands() {
        getCommand("randomtp").setExecutor(new RandomTpCommand(randomTpManager));
        getCommand("reclaim").setExecutor(new ReclaimCommand(reclaimManager, corePlugin.getPlayerDataStorage()));
        getCommand("resetreclaim").setExecutor(new ResetReclaimCommand(corePlugin.getPlayerDataStorage()));
        getCommand("shop").setExecutor(new ShopCommand(corePlugin.getGuiManager(), shopManager));
    }

    private void registerListeners(FileConfiguration config) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new RandomTpListener(randomTpManager), this);
        pluginManager.registerEvents(new DropListener(dropManager), this);
        pluginManager.registerEvents(new ItemExplodeListener(), this);
        pluginManager.registerEvents(new VillagerListener(config), this);
        pluginManager.registerEvents(new ApplesListener(config), this);
        pluginManager.registerEvents(new FirstJoinListener(corePlugin.getPlayerDataStorage()), this);
    }

    @SuppressWarnings("SameParameterValue")
    private Plugin grabDependency(String name) {
        Plugin plugin = Bukkit.getPluginManager().getPlugin(name);
        if(plugin == null || !plugin.isEnabled()) {
            getLogger().log(Level.SEVERE, "Could not find " + name + ", disabling...");
            getServer().getPluginManager().disablePlugin(this);
        }
        return plugin;
    }

}
