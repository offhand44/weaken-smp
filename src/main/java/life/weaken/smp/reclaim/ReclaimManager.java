package life.weaken.smp.reclaim;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

public class ReclaimManager {

    private final Map<String, Reclaim> reclaimMap;

    public ReclaimManager(Plugin plugin) {
        FileConfiguration config = plugin.getConfig();
        reclaimMap = new HashMap<>();
        ConfigurationSection reclaimSection = config.getConfigurationSection("reclaim");
        if(reclaimSection == null) return;

        for(String string : reclaimSection.getKeys(false)) {
            ConfigurationSection keys = reclaimSection.getConfigurationSection(string + ".keys");
            if(keys == null) continue;
            reclaimMap.put(string, new Reclaim(string, reclaimSection.getInt(string + ".weight"), keys));
        }
    }

    public Reclaim getReclaimFor(Player player) {
        Reclaim reclaim = null;
        for(String string : reclaimMap.keySet()) {
            if(!player.hasPermission("smp.reclaim." + string)) continue;
            Reclaim current = reclaimMap.get(string);
            if(reclaim == null || current.getWeight() > reclaim.getWeight()) {
                reclaim = current;
            }
        }
        return reclaim;
    }

}
