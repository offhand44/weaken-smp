package life.weaken.smp.reclaim;

import life.weaken.core.playerdata.PlayerDataStorage;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import su.nightexpress.goldencrates.api.GoldenCratesAPI;
import su.nightexpress.goldencrates.manager.key.CrateKey;

import java.util.Map;

public record ReclaimCommand(ReclaimManager manager, PlayerDataStorage storage) implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "Only players can use this command");
            return true;
        }

        YamlConfiguration config = storage.get(player);
        long lastReclaim = config.getLong("last-reclaim");

        if(lastReclaim != 0L) {
            sender.sendMessage(ChatColor.RED + "You have already claimed your reclaim this season");
            return true;
        }

        Reclaim reclaim = manager().getReclaimFor(player);
        if(reclaim == null) {
            sender.sendMessage(ChatColor.RED + "Your rank does not have a reclaim!");
            return true;
        }

        Map<CrateKey, Integer> keyMap = reclaim.getKeys();
        for(CrateKey key : keyMap.keySet()) {
            GoldenCratesAPI.getKeyManager().giveKey(player, key, keyMap.get(key));
        }

        sender.sendMessage(ChatColor.GREEN + "You have redeemed your " + reclaim.getName() + " rank reclaim");
        config.set("last-reclaim", System.currentTimeMillis());
        return true;
    }
}
