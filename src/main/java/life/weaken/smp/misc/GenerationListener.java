package life.weaken.smp.misc;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class GenerationListener implements Listener {

    private Map<Material, Material> replacementMap;
    private final Plugin plugin;

    public GenerationListener(Plugin plugin) {
        replacementMap = new HashMap<>();
        this.plugin = plugin;
        load(plugin.getConfig());
    }

    @EventHandler
    public void onGenerate(ChunkLoadEvent event) {
        if(event.isNewChunk()) processChunk(event.getChunk());
    }

    public void processChunk(Chunk chunk) {
        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                for(int y = 0; y < 256; y++) {
                    Block block = chunk.getBlock(x, y, z);

                    if(replacementMap.containsKey(block.getType())) {
                        block.setType(replacementMap.get(block.getType()), false);
                    }

                }
            }
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void load(FileConfiguration config) {
        Map<Material, Material> replacements = new HashMap<>();
        ConfigurationSection generationSection = config.getConfigurationSection("generation-replacements");
        if(generationSection == null) return;

        for(String replacement : generationSection.getKeys(false)) {
            Material from, to;
            try {
                from = Material.valueOf(replacement.toUpperCase());
                to = Material.valueOf(generationSection.getString(replacement).toUpperCase());
            } catch(IllegalArgumentException e) {
                plugin.getLogger().log(Level.WARNING, "Invalid materials in block replacement map: " +
                        replacement.toUpperCase() + ", " + generationSection.getString(replacement).toUpperCase());
                continue;
            }

            replacements.put(from, to);
        }

        replacementMap = replacements;
    }

}
