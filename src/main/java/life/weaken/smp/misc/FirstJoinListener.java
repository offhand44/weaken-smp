package life.weaken.smp.misc;

import life.weaken.core.playerdata.PlayerDataStorage;
import life.weaken.smp.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public record FirstJoinListener(PlayerDataStorage storage) implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        YamlConfiguration config = storage.get(event.getPlayer());
        if(config.getLong("first-join") == 0L) {
            config.set("first-join", System.currentTimeMillis());
            event.getPlayer().teleport(new Location(Bukkit.getWorld("world"), -6.5, 66, -6.5));
            event.getPlayer().getInventory().addItem(new ItemBuilder(Material.COOKED_BEEF).setAmount(64).create());
            event.getPlayer().getInventory().addItem(new ItemBuilder(Material.IRON_AXE).setAmount(1).create());
            event.getPlayer().getInventory().addItem(new ItemBuilder(Material.OAK_WOOD).setAmount(16).create());
        }
    }

}
