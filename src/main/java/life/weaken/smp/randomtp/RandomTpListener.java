package life.weaken.smp.randomtp;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public record RandomTpListener(RandomTpManager manager) implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(event.getClickedBlock() == null) return;
        Player player = event.getPlayer();

        for(Location location : manager.getBlocks()) {
            if(location.equals(event.getClickedBlock().getLocation())) {
                player.sendMessage(ChatColor.GREEN + "Teleporting...");
                manager.teleport(player);
                event.setCancelled(true);
                return;
            }
        }
    }

}
