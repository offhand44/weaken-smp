package life.weaken.smp.shop;

import life.weaken.core.gui.GuiManager;
import life.weaken.smp.shop.gui.ShopGui;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public record ShopCommand(GuiManager guiManager, ShopManager shopManager) implements CommandExecutor {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be used by players");
            return true;
        }

        guiManager.showGui(player, new ShopGui(guiManager, shopManager));
        return true;
    }

}
