package life.weaken.smp.shop;

import life.weaken.smp.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

public class ShopItem {

    private final float cost;
    private final ItemStack item;

    public ShopItem(String item, ConfigurationSection section) {

        cost = (float) section.getDouble("cost");

        Material material = null;
        try {
            material = Material.valueOf(item.toUpperCase());
        } catch(IllegalArgumentException e) {
            e.printStackTrace();
        }

        this.item = new ItemBuilder(material)
                .setAmount(section.getInt("amount"))
                .create();
    }

    public float getCost() {
        return cost;
    }

    public ItemStack getItem() {
        return item.clone();
    }

}
