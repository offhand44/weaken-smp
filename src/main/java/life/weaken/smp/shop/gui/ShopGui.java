package life.weaken.smp.shop.gui;

import life.weaken.core.gui.GuiManager;
import life.weaken.core.gui.guis.Gui;
import life.weaken.smp.shop.ShopItem;
import life.weaken.smp.shop.ShopManager;

public class ShopGui extends Gui {

    public ShopGui(GuiManager manager, ShopManager shopManager) {
        super(manager, "Shop", 3);

        int i = 0;
        for(ShopItem item : shopManager.getShopItems()) {
            addItem(i, new ShopGuiItem(this, shopManager.getEconomy(), item));
            i++;
        }
    }

}
